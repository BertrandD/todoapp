todoApp.controller('TodoCtrl', ['$scope', '$rootScope', function($scope, $rootScope) {
  $scope.todos = [];

    io.socket.get('/todo', function (resData) {
    $scope.$apply(function(){
      $scope.todos = resData;
    });
    io.socket.on("todo", function(event){
      console.log(event);
      if(event.verb == "created"){
        $scope.$apply(function(){
          $scope.todos.push(event.data);
        });
      }else if(event.verb == "destroyed"){
        $scope.$apply(function(){
          console.log($scope.todos);
          for (var i = 0; i < $scope.todos.length; i++) {
            if($scope.todos[i].id == event.previous.id){
              $scope.todos.splice(i, 1);
              i = $scope.todos.length;
            }
          }
        });
      }
    })
  });

 
  $scope.addTodo = function() {
    if($scope.newTodo.trim()){
      io.socket.post('/todo', { value: $scope.newTodo }, function (resData) {
        $scope.$apply(function(){
          $scope.newTodo = "";
          $scope.todos.push(resData);
        });
      });
    }
  }

  $scope.removeTodo = function(todo) {
    io.socket.delete('/todo/'+todo.id, function (resData) {
      $scope.$apply(function(){
        $scope.todos.splice($scope.todos.indexOf(todo), 1);
      });           
    });                                                        
  }
}]);